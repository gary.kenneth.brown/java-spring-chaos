package uk.nhs.nhsbsa.poc.chaos;

import org.springframework.stereotype.Service;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
public class SomeService implements ISomeService {

	public String execute() {
		try {
			Thread.sleep(100);
		} catch (InterruptedException e) {
			log.error("Something is wrong", e);
			throw new ExampleException("oh dear");
		}
		return "foo";
	}
}
