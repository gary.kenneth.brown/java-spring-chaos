package uk.nhs.nhsbsa.poc.chaos;

public class ExampleException extends RuntimeException {

	public ExampleException(String msg) {
		super(msg);
	}

}
