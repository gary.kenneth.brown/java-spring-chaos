package uk.nhs.nhsbsa.poc.chaos;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import lombok.NoArgsConstructor;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@RestController
@NoArgsConstructor(onConstructor = @__(@Autowired))
public class SomeController {
	
	@Autowired
	private ISomeService service;
	
	@GetMapping("/")
	public String root() {
		return service.execute();
	}
}
