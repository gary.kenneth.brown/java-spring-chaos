package uk.nhs.nhsbsa.poc.chaos;

import com.intuit.karate.junit5.Karate;

public class ChaosTests {
	
	@Karate.Test
	Karate testAll() {
		return Karate.run().relativeTo(getClass());
	}
}
