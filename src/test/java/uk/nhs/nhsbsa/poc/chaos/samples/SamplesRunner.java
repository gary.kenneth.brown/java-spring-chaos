package uk.nhs.nhsbsa.poc.chaos.samples;

import com.intuit.karate.junit5.Karate;

public class SamplesRunner {

	@Karate.Test
    Karate testSample() {
        return Karate.run("samples").relativeTo(getClass());
    }
}