# Chaos Engineering with Spring Boot
Project to experiment with Spring Boot Chaos Monkey and testing it using Karate and JMeter.
Micrometer has been added to monitor how the application is responding sending metrics and JMeter tests output to InfluxDB.

# Chaos Monkey
Chaos Monkey instruments failures and latency to calls within the Java application. The affected components can be specified using their Spring annotations:
`@Service` `@Controller` `@Repository` `@Component`.

More details are on the [project page](https://codecentric.github.io/chaos-monkey-spring-boot/).

## Setup
Add to the Maven `pom.xml` the following:

```xml
<dependency>
	<groupId>de.codecentric</groupId>
	<artifactId>chaos-monkey-spring-boot</artifactId>
	<version>2.1.1</version>
</dependency>
```

It is useful to include Spring Boot Actuator for the ChaosMonkey actuator.

```xml
<dependency>
	<groupId>org.springframework.boot</groupId>
	<artifactId>spring-boot-starter-actuator</artifactId>
</dependency>
```

Your application.properties or yml will need for the Chaos monkey and actuator:

```
chaos.monkey.assaults.level=5
chaos.monkey.assaults.latencyActive=true
chaos.monkey.assaults.latencyRangeStart=1000
chaos.monkey.assaults.latencyRangeEnd=30000
chaos.monkey.assaults.exceptionsActive=true
chaos.monkey.assaults.memoryActive=true
chaos.monkey.assaults.killApplicationActive=false

chaos.monkey.watcher.controller=false
chaos.monkey.watcher.restController=true
chaos.monkey.watcher.service=true
chaos.monkey.watcher.repository=false
chaos.monkey.watcher.component=true

management.endpoints.web.exposure.include=health,info,chaosmonkey
```

# Running
Start the application with the Spring profile (provided by the library) `chaos-monkey`

```
java -jar <jarname>.jar --spring.profiles.active=chaos-monkey
```

You can control the Chaos Monkey using the Actuator API:

```
curl -X POST http://localhost:8080/actuator/chaosmonkey/enable

curl -X POST http://localhost:8080/actuator/chaosmonkey/disable

curl http://localhost:8080/actuator/chaosmonkey/status
```

# Chaos Toolkit
[Chaos Toolkit website](https://docs.chaostoolkit.org/) - provides a framework for Chaos engineering across a platform. It integrates with Spring Chaos Monkey via the Spring Actuator.

Example chaos tests:
* application latency
* java app failure
* AWS database node / cluster failure
* AWS EC2 failure
* SSL certificate expiry / invalid signer

# Karate
The [Karate](https://github.com/intuit/karate) tests the API exposed by the sample application.

```xml
<properties>
     <karate.version>0.9.5</karate.version>
</properties>
     
...
     
<dependency>
	<groupId>com.intuit.karate</groupId>
	<artifactId>karate-apache</artifactId>
	<version>${karate.version}</version>
	<scope>test</scope>
</dependency>
<dependency>
	<groupId>com.intuit.karate</groupId>
	<artifactId>karate-junit5</artifactId>
	<version>${karate.version}</version>
	<scope>test</scope>
</dependency>
```

## Karate setup structure
With the way you setup Karate and the cucumber `.feature` files are placed in the same folder and package as the Java JUnit Runner class for that test. More details are (here](https://github.com/intuit/karate#folder-structure).

Updating the maven pom.xml to include **resources** from the `src/main/java` folder, ignoring the java JUnit test classes, will copy the `.feature` files over into the target `test-classes` folder.
```
<build>
	<testResources>
		<testResource>
			<directory>src/test/java</directory>
			<excludes>
				<exclude>**/*.java</exclude>
			</excludes>
		</testResource>
	</testResources>
</build>
```

## Running karate
Run the tests as normal JUnit or maven lifecycle `mvn test`.

# JMeter
JMeter has been added to test the application when chaos monkey is enabled and to see how it affects the application.
An example JMeter [script](Test%20Plan.jmx) has been provided.

## Test plans
This was [useful](https://jmeter.apache.org/usermanual/build-web-test-plan.html) to setup a web test plan streaming results to InfluxDB/Graphite.

## Setup and Execution
1. JMeter can be downloaded [here](https://jmeter.apache.org/download_jmeter.cgi)
2. Unzip JMeter to your workstation `c:\program files\apache-jmeter-5.2.1`
3. Update workstation environment variables `set PATH=%PATH%;c:\program files\apache-jmeter-5.2.1\bin`
4. Run jmeter in the command line using `jmeter`

# InfluxDB
A time series database to store metrics and execution data from the Spring Boot application. It has an extension to serve a Graphite API which JMeter supports.

1. Download influxDB using Chocolately: `choco install influxdb`
2. Run the influxdb daemon - _Note: McAfee runs a service on tcp/8086. Change the influxdb.conf so http is exposed on tcp/9086 instead._ `influxd.exe -config influxdb.conf`
3. Connect to influx using the command line and create a database for the application metrics `chaosexample` and jmeter `jmeter`:

```
influx.exe -port 9086
> create database jmeter
> create database chaosexample
> quit
```

4. Edit the influxdb.conf again and uncomment and change the **graphite** section as follows:

``` 
[[graphite]]
  # Determines whether the graphite endpoint is enabled.
  enabled = true
  database = "jmeter"
  retention-policy = ""
  bind-address = ":2003"
  protocol = "tcp"
  consistency-level = "one"

  # Flush if this many points get buffered
  batch-size = 5000

  # number of batches that may be pending in memory
  batch-pending = 10

  # Flush at least this often even if we haven't hit buffer limit
  batch-timeout = "1s"

  # UDP Read buffer size, 0 means OS default. UDP listener will fail if set above OS max.
  udp-read-buffer = 0

  ### This string joins multiple matching 'measurement' values providing more control over the final measurement name.
  separator = "."
```

5. Restart the influxdb daemon and check graphite is running on `tcp/2003` in the log file.

This was a [useful](https://www.blazemeter.com/blog/how-to-use-grafana-to-monitor-jmeter-non-gui-results) document to setup JMeter, InfluxDB and Grafana to visualise the application.

## Spring Boot Micrometer InfluxDB

1. Add the following to Maven:

```xml
<dependency>
	<groupId>io.micrometer</groupId>
	<artifactId>micrometer-registry-influx</artifactId>
</dependency>
```

2. Add configuration to `application.properties`

```
management.metrics.export.influx.uri=http://localhost:9086
management.metrics.export.influx.db=chaosexample
management.metrics.export.influx.auto-create-db=true
```

This will output a number of JVM, Chaos Monkey and Tomcat metrics to InfluxDB for monitoring and alerting.

# Grafana
Visualisation of metrics from the application and jmeter via InfluxDB.

1. Install using Windows Chocolately: `choco install grafana`
2. CD `c:\ProgramData\chocolatey\lib\grafana\tools\grafana-6.3.6\bin` and run `grafana-server.exe`
3. Login using the default `admin` and `admin` credentials. You will be asked to reset the password - `grafana` is fine for local dev.
4. setup a datasource to the InfluxDB `http://localhost:9086` and database `chaosexample`
5. Setup a dashboard and add monitors. Sample dashboard [configuration](grafana-dashboard.json)

# Other Resources
[Chaos Engineering - Tools and Tutorials](https://techbeacon.com/app-dev-testing/chaos-engineering-testing-34-tools-tutorials)
